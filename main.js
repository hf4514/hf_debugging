import App from './App'
import {HFdebugging} from '@/uni_modules/HF-HF_debugging/common/next.js'

// #ifdef APP
// import {HFdebugging} from '@/uni_modules/HF-HF_debugging/common/index.js'
// #endif

// #ifndef VUE3
import Vue from 'vue'
Vue.config.productionTip = false
App.mpType = 'app'
const app = new Vue({
    ...App
})
app.$mount()
new HFdebugging()
// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'
export function createApp() {
  const app = createSSRApp(App)
  new HFdebugging({app});
  return {
    app
  }
}
// #endif